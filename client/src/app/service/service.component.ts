import { Steps } from './../models/Steps';
import { ApiService } from './../services/api/api.service';
import { Component, OnInit } from '@angular/core';
import { Services} from './../models/Services';
import { Requirments } from 'app/models/Requirments';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: 'service-cmp',
    moduleId: module.id,
    templateUrl: 'service.component.html'
})

export class ServiceComponent implements OnInit {
    _steps: Steps;
    _requirmnets: Requirments;
    Link: any;
    constructor(private api: ApiService , private route:  ActivatedRoute) {

    }
    getRequirment( id: String) {
        this.api.get('/services/' + id + '/requirment', '')
            .subscribe(
                res => {
                    this._requirmnets = res;
                },
                err => {
                    console.log(err);
                }
            );
    }

    getSteps( id: String) {
        this.api.get('/services/' + id + '/steps', '' )
            .subscribe(
                res => {
                    this._steps = res;
                },
                err => {
                    console.log(err);
                }
            );
    }
    ngOnInit() {
        this.route.queryParams.forEach((params: Params) => {
            this.Link = params['id'];
        });
        this.getRequirment(this.Link);
        this.getSteps(this.Link);
    }
}
