import { Component, OnInit} from '@angular/core';
import { ApiService } from 'app/services/api/api.service';
import { Services } from 'app/models/services';

declare var $:any;

@Component({
    selector: 'dashboard-cmp',
    moduleId: module.id,
    templateUrl: 'dashboard.component.html'
})

export class DashboardComponent implements OnInit   {
    _services: Services;
    constructor(private api: ApiService) {

    }
    getRequest() {
        this.api.get('services', '')
            .subscribe(
                res => {
                    this._services = res;
                },
                err => {
                    console.log(err);
                }
            );
    }
    ngOnInit() {
        this.getRequest();
    }
}
