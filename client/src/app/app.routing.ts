import { FeedbackComponent } from './feedback/feedback.component';
import { Routes } from '@angular/router';

import { DashboardComponent }   from './dashboard/dashboard.component';
import {ServiceComponent } from './service/service.component';

export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    },
    {
        path: 'dashboard',
        component: DashboardComponent
    },
    {
        path: 'service',
        component: ServiceComponent
    },
    {
        path: 'feedback',
        component: FeedbackComponent
    }
]
