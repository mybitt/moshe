import { ActivatedRoute, Router } from '@angular/router';
import { Feedback } from './../models/Feedback';
import { ApiService } from './../services/api/api.service';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-feedback',
    moduleId: module.id,
    templateUrl: 'feedback.component.html'
})

export class FeedbackComponent implements OnInit {
    _feedback: Feedback;
    _satisfied: boolean = true;
    thanks: String;
    constructor(private api: ApiService , private route : Router) {

    }
    setText(message: String, element) {
        element.value = message;
        if (message == 'አዎ') {
            this._satisfied = true;
        } else{
            this._satisfied = false;
        }
    }
    postFeedback(element) {
        let data = { 'message': element.value , 'satisfied': this._satisfied }
        this.api.post('feedbacks', data)
            .subscribe(
                res => {
                    this.finish(res);
                },
                err => {
                    console.log(err);
                }
            );
    }
    finish(res) {
        this._feedback = res;
        if (res != null) {
            this.thanks = "ለአስተያየትዎ እናመሰግናለን";
            setTimeout(() => this.route.navigate(['/dashboard']),3000);
        }
    }

    ngOnInit(): void {
    }
}
