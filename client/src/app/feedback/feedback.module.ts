import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FeedbackComponent } from './feedback.component';

@NgModule({
    imports: [ RouterModule, CommonModule ],
    declarations: [ FeedbackComponent ],
    exports: [ FeedbackComponent ]
})

export class FeedbackModule {
    
}
