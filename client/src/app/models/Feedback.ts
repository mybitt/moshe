import { Injectable } from '@angular/core';

@Injectable()
export class Feedback {
    id: String;
    name: String;
}
