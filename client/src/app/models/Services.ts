import { Injectable } from '@angular/core';

@Injectable()
export class Services {
    name: String;
    id: String;
    description: String;
}
