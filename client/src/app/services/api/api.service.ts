import { Injectable } from '@angular/core';
//import {Observable} from 'rxjs/Observable';
import {Http} from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {HttpClient , HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class ApiService {

  baseUrl = '/api/';
  //baseUrl = 'http://:3000/api/';
  constructor(private http: HttpClient) { }

  // get all models
  get(model: string, filter: string): Observable<any> {
    return this.http
      .get(this.baseUrl + model + '?filter=' + filter)
      .map((response: any) => {
        return <any> response;
      })
      .catch(this.handleError);
  }

  // get with out filter
  getWithNoFilter(model: string): Observable<any> {
    return this.http
      .get(this.baseUrl + model)
      .map((response: any) => {
        return <any> response;
      })
      .catch(this.handleError);
  }

  // get all models by id
  getById(model: string, id: string, filter: string): Observable<any> {
    return this.http
      .get(this.baseUrl + model + '/' + id + '?filter=' + filter)
      .map((response: any) => {
        return <any> response;
      })
      .catch(this.handleError);
  }

  login(data) {
    return this.http
      .post(this.baseUrl + 'Users/login', data)
      .map((response: any) => {
        return response;
      })
      .catch(this.handleError);
  }
  // patch all models
  patch(model, data, id, token) {
    return this.http
      .patch(this.baseUrl + model + '/' + id + '?access_token=' + token, data)
      .map((response: any) => {
        return response;
      })
      .catch(this.handleError);
  }

  clientLogin(email, password, serviceName) {
    return this.http
    // tslint:disable-next-line:max-line-length
      .get(this.baseUrl + 'clients?filter={"where": {"email": "' + email + '", "password": "' + password + '", "serviceName": "' + serviceName + '"}, "include": "cardDetails", "limit": 1}')
      .map((response: any) => {
        return <any> response;
      })
      .catch(this.handleError);
  }

  // post all models
  post(model , data) {
    return this.http
      .post(this.baseUrl + model, data)
      .map((response: any) => {
        return response;
      })
      .catch(this.handleError);
  }

  postAll(model , data) {
    return this.http
      .post(this.baseUrl + model, data)
      .map((response: any) => {
        return response;
      })
      .catch(this.handleError);
  }

  // delete by id
  delete(model , id) {
    return this.http
      .delete(this.baseUrl + model + '/' + id)
      .map((response: any) => {
        return response;
      })
      .catch(this.handleError);
  }

  // lets see what's going on
  private handleError(error: any) {
    console.log( error);
    return Observable.throw(error.statusText);
  }

}

