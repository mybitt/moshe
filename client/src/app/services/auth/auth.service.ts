import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Http , Response } from '@angular/http';

@Injectable()
export class AuthService {

  baseUrl = 'http://localhost:3000/api/';
  static logged: boolean = false;
  constructor(private http: Http) {

  }


  // login
  login(data) {
    return this.http
      .post(this.baseUrl + 'Users/login', data)
      .map((response: Response) => {
        return response.json();
      })
      .catch(this.handleError);
  }

  // logout
  logout() {
    return this.http
      .post(this.baseUrl + 'Users/logout', [])
      .map((response: Response) => {
        return response.json();
      })
      .catch(this.handleError);
  }

  // register
  register(data) {
    return this.http
      .post(this.baseUrl + 'Users', data)
      .map((response: Response) => {
        return response.json();
      })
      .catch(this.handleError);
  }

  private handleError(error: Response) {
    console.log('error from the api service ' + error);
    return Observable.throw(error.statusText);
  }

}
